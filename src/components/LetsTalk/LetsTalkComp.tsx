import { Button } from 'antd'
import React from 'react'

function LetsTalk() {
  const vectorLeft = {
    backgroundImage: "url('./static/images/vectors/rectangleLeft.svg')",
  }

  const containerStyles = {
    boxShadow:
      '0px 30px 40px 10px rgba(223, 30, 54, 0.1)',
  }
  return (
    <div className="mx-[140px] bg-[#DF1E36] rounded-2xl px-[60px] py-[75px]" style={containerStyles}>
      <div className="flex justify-between items-center">
        <div>
          <div className="relative">
            <div
              className="absolute top-[-75px] rounded-2xl left-[-60px] w-[466px] bg-no-repeat bg-contain h-[275px]"
              style={vectorLeft}
            />
            <div className="flex items-center justify-start gap-x-2 relative">
              <h1 className="text-4xl font-semibold text-white rounded-3xl z-10 relative">
                Lets Talk!
              </h1>
              <img
                src="./static/images/icons/chat-bubble.svg"
                alt="chat-icon"
                className="-mt-2 z-10 relative"
              />
            </div>
            <div>
              <p className="text-[16px] text-[#fff] relative z-[10]">
                Punya project atau ingin bekerjasama?
              </p>
              <p className="text-[16px] text-[#fff] relative z-10">
                Hubungi kami dan mulai bangun solusi bisnis anda sekarang!
              </p>
            </div>
          </div>
        </div>
        <Button className="flex bg-[#404258] gap-2 w-[182px] h-[59px] py-[16px] px-[24px] border-none rounded-lg z-10">
          <p className="font-semibold text-[16px] text-[#fff] z-10">
            Get In Touch{' '}
          </p>
          <div className="flex items-center z-10">
            <img
              src="./static/images/icons/arrow-icon.svg"
              alt="arrow"
              className="w-6 z-10"
            />
          </div>
          </Button>
      <div
        className="absolute rounded-2xl right-[142px] w-[466px] bg-no-repeat bg-contain h-[275px] scale-x-[-1] "
        style={vectorLeft}
        />
      </div>
    </div>
  )
}

export default LetsTalk
